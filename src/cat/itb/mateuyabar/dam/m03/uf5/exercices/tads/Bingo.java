package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Bingo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<Integer> bingoCard = new HashSet<>();
        for (int i = 0; i < 10; i++) {
            int value = scanner.nextInt();
            bingoCard.add(value);
        }

        while(!bingoCard.isEmpty()){
            int value = scanner.nextInt();
            bingoCard.remove(value);
            System.out.printf("Et queden %d números%n", bingoCard.size());
        }
        System.out.println("BINGO!");
    }
}
