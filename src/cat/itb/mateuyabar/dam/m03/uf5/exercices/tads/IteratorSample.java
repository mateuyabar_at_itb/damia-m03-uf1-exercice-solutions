package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class IteratorSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()){
            int value = iterator.next();
            if(value%2==0)
                iterator.remove();
        }
        System.out.println(list);
    }
}
