package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class LamdaSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = IntegerLists.readIntegerList(scanner);
        values.removeIf(value -> value%10==3);
        values.sort((v1, v2) -> v2-v1);
        values.forEach(value -> System.out.println(value));
    }
}
