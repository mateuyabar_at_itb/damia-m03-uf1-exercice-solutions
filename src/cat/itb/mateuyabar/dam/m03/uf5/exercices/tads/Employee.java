package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.Objects;

public class Employee {
    String name;
    String surname;
    String dni;
    String address;

    public Employee(String name, String surname, String dni, String address) {
        this.name = name;
        this.surname = surname;
        this.dni = dni;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getDni() {
        return dni;
    }

    public String getAddress() {
        return address;
    }

}

