package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

public class Patient implements Comparable<Patient>{
    String name;
    int priority;

    public Patient(String name, int priority) {
        this.name = name;
        this.priority = priority;
    }

    public String getName() {
        return name;
    }

    public int getPriority() {
        return priority;
    }

    @Override
    public int compareTo(Patient patient) {
        return patient.priority-priority;
    }
}
