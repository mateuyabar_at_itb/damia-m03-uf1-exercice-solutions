package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import cat.itb.mateuyabar.dam.m03.uf5.exercices.tads.Employee;
import cat.itb.mateuyabar.dam.m03.uf5.exercices.tads.EmployeeById;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class FilterEmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int count = scanner.nextInt();
        List<Employee> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Employee employee = EmployeeById.readEmployee(scanner);
            list.add(employee);
        }

        list.removeIf(FilterEmpoyeeById::dniContainsA);
        list.forEach(System.out::println);
    }

    private static boolean dniContainsA(Employee employee) {
        return employee.getDni().contains("A");
    }
}
