package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EmployeeById {
    public static void main(String[] args) {
        // dni -> employee
        Scanner scanner = new Scanner(System.in);
        Map<String, Employee> employees = readEmployeesMap(scanner);
        printEmployees(scanner, employees);
    }
    private static void printEmployees(Scanner scanner, Map<String, Employee> employees) {
        String dni = scanner.nextLine();
        while(!dni.equals("END")){
            Employee employee = employees.get(dni);
            System.out.println(employee);
            dni = scanner.nextLine();
        }
    }
    private static Map<String, Employee> readEmployeesMap(Scanner scanner) {
        Map<String, Employee> employees = new HashMap<>();

        int count = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < count; i++) {
            Employee employee = readEmployee(scanner);
            employees.put(employee.dni, employee);
        }
        return employees;
    }

    public static Employee readEmployee(Scanner scanner) {
        String dni = scanner.nextLine();
        String name = scanner.nextLine();
        String surname = scanner.nextLine();
        String address = scanner.nextLine();
        return new Employee(name, surname, dni, address);
    }
}
