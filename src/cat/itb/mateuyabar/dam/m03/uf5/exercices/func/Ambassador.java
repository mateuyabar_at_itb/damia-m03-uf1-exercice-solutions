package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

public class Ambassador {
    String name;
    String surnames;
    Country country;

    public Ambassador(String name, String surnames, Country country) {
        this.name = name;
        this.surnames = surnames;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public int getCountryArea() {
        return country.getArea();
    }

    @Override
    public String toString() {
        return "Ambassador{" +
                "name='" + name + '\'' +
                ", surnames='" + surnames + '\'' +
                ", country=" + country +
                '}';
    }
}
