package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class NearestTo100 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);

        Comparator<Integer> comparator = Comparator.comparing(NearestTo100::distanceFrom100);
        int nearest = Collections.min(list, comparator);
        int farthest = Collections.max(list, comparator);
        System.out.println("Nearest 100: "+nearest);
        System.out.println("Farthest 100: "+farthest);
    }

    public static int distanceFrom100(int value){
        return Math.abs(value-100);
    }
}
