package cat.itb.mateuyabar.dam.m03.uf5.exercices.regex;

import cat.itb.mateuyabar.dam.m03.uf2.dataclasses.School;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadSubjectInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < count; i++) {
            processLine(scanner);
        }
    }

    private static void processLine(Scanner scanner) {
        String line = scanner.nextLine();
        // DAM-M03UF2
        String regex = "(.+)-M(\\d+)UF(\\d+)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(line);
        while(matcher.find()){
            String cicle = matcher.group(1);
            String module = matcher.group(2);
            String uf = matcher.group(3);
            System.out.println(cicle+" "+module+" "+uf);
        }
    }
}
