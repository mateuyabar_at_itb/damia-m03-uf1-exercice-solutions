package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import java.util.*;

import static cat.itb.mateuyabar.dam.m03.uf5.exercices.func.CountryDataSortByName.readCountries;

public class EmbassatorsByBiggestCountry {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countries = readCountries(scanner);
        Map<String, Country> countryMap = new HashMap<>();
        for(Country country: countries){
            countryMap.put(country.getName(), country);
        }
        List<Ambassador> ambassadors = readAmbassadors(scanner, countryMap);
        ambassadors.sort(Comparator.comparing(Ambassador::getCountryArea)
                .thenComparing(Ambassador::getSurnames)
                .thenComparing(Ambassador::getName)
        );
        ambassadors.forEach(System.out::println);
    }

    private static List<Ambassador> readAmbassadors(Scanner scanner, Map<String, Country> countryMap) {
        List<Ambassador> ambassadors = new ArrayList<>();
        int count = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < count; i++) {
            String name = scanner.nextLine();
            String surnames = scanner.nextLine();
            String countryName = scanner.nextLine();
            Country country = countryMap.get(countryName);
            ambassadors.add(new Ambassador(name, surnames, country));
        }
        return ambassadors;
    }
}
