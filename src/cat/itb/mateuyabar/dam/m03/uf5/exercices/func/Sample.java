package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import cat.itb.mateuyabar.dam.m03.uf4.samples.Rectangle;
import cat.itb.mateuyabar.dam.m03.uf5.exercices.tads.Employee;

import java.util.ArrayList;
import java.util.List;

public class Sample {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        int value = list.get(3);

        value = getThird(list);

        List<Rectangle> list2 = new ArrayList<>();
        Rectangle rectangle = getThird(list2);

        Box<Rectangle> box = new Box<>(new Rectangle(2,2));
        Box<Integer> box2 = new Box<>(2);
    }

    public static <T> T getThird(List<T> list){
        T value =  list.get(3);
        return value;
    }
}
