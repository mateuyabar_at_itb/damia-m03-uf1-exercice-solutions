package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MetersToCentimeters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> distancesInMeters = IntegerLists.readIntegerList(scanner);

        distancesInMeters.stream()
                .map(distance -> distance*100)
                .forEach(System.out::println);

    }
}
