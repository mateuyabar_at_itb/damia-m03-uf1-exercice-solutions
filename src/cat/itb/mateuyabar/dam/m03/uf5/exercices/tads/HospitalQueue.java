package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class HospitalQueue {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Queue<Patient> patients = new PriorityQueue<>();

        int operation = scanner.nextInt();
        while(operation!=-1) {
            switch (operation) {
                case 1:
                    addPatient(scanner, patients);
                    break;
                case 0:
                    patientToHospital(patients);
            }
            operation = scanner.nextInt();
        }
    }

    private static void patientToHospital(Queue<Patient> patients) {
        Patient patient = patients.poll();
        System.out.printf("%s passi a consulta%n", patient.getName());
    }

    private static void addPatient(Scanner scanner, Queue<Patient> patients) {
        int priority = scanner.nextInt();
        String name = scanner.nextLine();
        Patient patient = new Patient(name, priority);
        patients.add(patient);
    }
}
