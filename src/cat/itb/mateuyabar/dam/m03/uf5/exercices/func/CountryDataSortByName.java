package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.IntStream;

public class CountryDataSortByName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countries = readCountries(scanner);

        countries.stream()
                .filter(country -> country.getDensity()>5)
                .sorted(Comparator.comparing(Country::getName))
                .forEach(System.out::println);

        // Option2
//        IntStream.range(0, count)
//                .mapToObj(i -> readCountry(scanner))
//                .filter(country -> country.getDensity()>5)
//                .sorted(Comparator.comparing(Country::getName))
//                .forEach(System.out::println);

    }

    public static List<Country> readCountries(Scanner scanner) {
        int count = scanner.nextInt();
        List<Country> countries = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Country country = readCountry(scanner);
            countries.add(country);
        }
        return countries;
    }

    private static Country readCountry(Scanner scanner) {
        String name = scanner.next();
        String capital = scanner.next();
        int area = scanner.nextInt();
        int density = scanner.nextInt();
        return new Country(name, capital, area, density);
    }
}
