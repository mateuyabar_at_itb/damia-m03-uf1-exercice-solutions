package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class CountryDataUpdateNames {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countries = CountryDataSortByName.readCountries(scanner);

        countries.stream().forEach(Country::toUpperCase);
        countries.stream().forEach(System.out::println);


    }


}
