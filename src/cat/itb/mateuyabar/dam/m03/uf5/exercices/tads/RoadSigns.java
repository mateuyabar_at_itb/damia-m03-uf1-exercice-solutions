package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RoadSigns {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<Integer, String> signs = readSigns(scanner);
        findSigns(scanner, signs);
    }

    private static void findSigns(Scanner scanner, Map<Integer, String> signs) {
        int meter = scanner.nextInt();
        while(meter!=-1){
            String sign = signs.getOrDefault(meter, "no hi ha cartell");
            System.out.println(sign);
            meter = scanner.nextInt();
        }
    }

    private static Map<Integer, String> readSigns(Scanner scanner) {
        Map<Integer, String> signs =new HashMap<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            int metre = scanner.nextInt();
            String sign = scanner.nextLine();
            signs.put(metre, sign);
        }
        return signs;
    }
}
