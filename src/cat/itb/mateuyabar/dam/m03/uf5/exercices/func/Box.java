package cat.itb.mateuyabar.dam.m03.uf5.exercices.func;

public class Box<T> {
    T value;

    public Box(T value) {
        this.value = value;
    }

    public T getValue() {
        return value;
    }
}
