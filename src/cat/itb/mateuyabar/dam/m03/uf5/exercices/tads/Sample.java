package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.*;

public class Sample {
    public static void main(String[] args) {

        Map<Integer, String> map = new HashMap<>();

        List<Integer> list = new LinkedList<>();
        for(int value: list){
            System.out.println(value);
        }

        Iterator<Integer> iterator = list.iterator();
        while(iterator.hasNext()) {
            int value = iterator.next();
            iterator.remove();
            System.out.println(value);
        }

    }
}
