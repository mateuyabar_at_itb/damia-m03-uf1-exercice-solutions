package cat.itb.mateuyabar.dam.m03.uf5.exercices.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SchoolDelegatVoting {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<String, Integer> votes = countVotes(scanner);
        printVotes(votes);
    }

    private static Map<String, Integer> countVotes(Scanner scanner) {
        Map<String, Integer> votes = new HashMap<>();
        String name = scanner.nextLine();
        while(!name.equals("END")) {
            if (!votes.containsKey(name)) {
                votes.put(name, 1);
            } else {
                int voteCount = votes.get(name) + 1;
                votes.put(name, voteCount);
            }
            name = scanner.nextLine();
        }
        return votes;
    }

    private static void printVotes(Map<String, Integer> votes) {
        for(String name: votes.keySet()){
            int voteCount = votes.get(name);
            System.out.println(name+": "+voteCount);
        }
    }
}
