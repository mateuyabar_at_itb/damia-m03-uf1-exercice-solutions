package cat.itb.mateuyabar.dam.m03.uf5.practica;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CovidStatsApp {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("src/cat/itb/mateuyabar/dam/m03/uf5/practica/coviddata.txt");
        Scanner scanner = new Scanner(path);
        List<CovidCountryData> datas = readDatas(scanner);

        printTotals(datas);
        // ...


    }


    private static void printTotals(List<CovidCountryData> datas) {
        int newCases = 0; // TODO
        int totalCases = 0; // TODO
        System.out.println("### DADES TOTALS ###");
        System.out.println("casos nous: "+newCases);
        System.out.println("casos totals: "+totalCases);
        System.out.println("");

    }

    private static List<CovidCountryData> readDatas(Scanner scanner) {
        List<CovidCountryData> list = new ArrayList<>();
        while(scanner.hasNext()){
            CovidCountryData covidCountryData = readData(scanner);
            list.add(covidCountryData);
        }
        return list;
    }

    private static CovidCountryData readData(Scanner scanner) {
        String country = scanner.nextLine();
        String countryCode = scanner.nextLine();
        int newCases = scanner.nextInt();
        int totalCases = scanner.nextInt();
        int newDeaths = scanner.nextInt();
        int totalDeaths = scanner.nextInt();
        int newRecovered = scanner.nextInt();
        int totalRecovered = scanner.nextInt();
        scanner.nextLine();
        return new CovidCountryData(country, countryCode, newCases, totalCases, newDeaths, totalDeaths, newRecovered, totalRecovered);
    }
}
