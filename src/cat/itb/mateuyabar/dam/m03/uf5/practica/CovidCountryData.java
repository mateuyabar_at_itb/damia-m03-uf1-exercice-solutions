package cat.itb.mateuyabar.dam.m03.uf5.practica;

public class CovidCountryData {
    String country;
    String countryCode;
    int newCases;
    int totalCases;
    int newDeaths;
    int totalDeaths;
    int newRecovered;
    int totalRecovered;

    public CovidCountryData(String country, String countryCode, int newCases, int totalCases, int newDeaths, int totalDeaths, int newRecovered, int totalRecovered) {
        this.country = country;
        this.countryCode = countryCode;
        this.newCases = newCases;
        this.totalCases = totalCases;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
    }

    public String getCountry() {
        return country;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public int getNewCases() {
        return newCases;
    }

    public int getTotalCases() {
        return totalCases;
    }

    public int getNewDeaths() {
        return newDeaths;
    }

    public int getTotalDeaths() {
        return totalDeaths;
    }

    public int getNewRecovered() {
        return newRecovered;
    }

    public int getTotalRecovered() {
        return totalRecovered;
    }
}
