package cat.itb.mateuyabar.dam.m03.uf3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WaitForInt {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            int value = scanner.nextInt();
            System.out.printf("Número: %d", value);
        } catch (InputMismatchException e){
            System.out.println("No és un enter");
        }
    }
}
