package cat.itb.mateuyabar.dam.m03.uf3;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class EssayAnalyser {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String pathString = scanner.nextLine();
        Path path = Paths.get(pathString);
        processFile(path);
    }

    private static void processFile(Path path) throws IOException {
        countLines(path);
        countWords(path);
    }

    private static void countWords(Path path) throws IOException {
        Scanner scanner = new Scanner(path);
        int count = 0;
        while (scanner.hasNext()){
            scanner.next();
            count++;
        }
        System.out.printf("Número de paraules: %d%n", count);
    }

    private static void countLines(Path path) throws IOException {
        Scanner scanner = new Scanner(path);
        int count = 0;
        // int countWord = 0;
        while(scanner.hasNext()){
            String line = scanner.nextLine();
            // countWord+= line.split(" ").length;
            count++;
        }
        System.out.printf("Número de línies: %d%n", count);

    }
}
