package cat.itb.mateuyabar.dam.m03.uf3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

/**
 * Backups .profile file
 */
public class ProfileBackup {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        Path profile = Path.of(homePath, ".profile");
        String date = LocalDateTime.now().toString();
        Path backupFolder = Path.of(homePath, "backup", date);
        Files.createDirectories(backupFolder);
        Path profileBackup = backupFolder.resolve(".profile");
        Files.copy(profile, profileBackup);
    }
}
