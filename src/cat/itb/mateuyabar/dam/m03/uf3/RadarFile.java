package cat.itb.mateuyabar.dam.m03.uf3;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists.readIntegerListFromFile;

public class RadarFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        List<Integer> speeds = readSpeeds(scanner);
        printData(speeds);
    }

    private static void printData(List<Integer> speeds) {
        int max = IntegerLists.max(speeds);
        int min = IntegerLists.min(speeds);
        double average = IntegerLists.avg(speeds);
        System.out.printf("Velocitat màxima: %dkm/h%n",max);
        System.out.printf("Velocitat mínima: %dkm/h%n",min);
        System.out.printf("Velocitat mitjana: %.1fkm/h%n",average);
    }

    private static List<Integer> readSpeeds(Scanner scanner) throws IOException {
        String pathString = scanner.nextLine();
        Path path = Paths.get(pathString);
        return readIntegerListFromFile(path);
    }


}
