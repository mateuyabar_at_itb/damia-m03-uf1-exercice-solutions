package cat.itb.mateuyabar.dam.m03.uf3;

import cat.itb.mateuyabar.dam.m03.uf2.classfun.Rectangle;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import static cat.itb.mateuyabar.dam.m03.uf2.classfun.RectangleSize.readRectangles;

public class RectangleToFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        List<Rectangle> rectangleList = readRectangles(scanner);
        String pathString= scanner.nextLine();
        Path path = Paths.get(pathString);
        save(rectangleList, path);
    }

    private static void save(List<Rectangle> rectangleList, Path path) throws IOException {
        try (OutputStream outputStream = Files.newOutputStream(path)) {
            PrintStream printStream = new PrintStream(outputStream, true);
            saveRectangles(rectangleList, printStream);
        }
    }

    private static void saveRectangles(List<Rectangle> rectangleList, PrintStream printStream) {
        for(Rectangle rectangle:rectangleList){
            saveRectangle(rectangle, printStream);
        }
    }

    private static void saveRectangle(Rectangle rectangle, PrintStream printStream) {
        printStream.println(rectangle);
    }
}
