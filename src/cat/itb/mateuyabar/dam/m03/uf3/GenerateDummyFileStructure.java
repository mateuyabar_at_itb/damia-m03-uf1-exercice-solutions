package cat.itb.mateuyabar.dam.m03.uf3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GenerateDummyFileStructure {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        Path dummyFolder = Paths.get(homePath, "dummyFolder");
        Files.createDirectories(dummyFolder);
        createSubFolders(dummyFolder);

    }

    private static void createSubFolders(Path dummyFolder) throws IOException {
        for(int i=1; i<=100; ++i){
            Path subFolder = dummyFolder.resolve(i+"");
            Files.createDirectories(subFolder);
        }
    }
}
