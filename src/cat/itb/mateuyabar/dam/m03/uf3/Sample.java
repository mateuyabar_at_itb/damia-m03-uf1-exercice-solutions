package cat.itb.mateuyabar.dam.m03.uf3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Sample {
    public static void main(String[] args) throws IOException {
        Path path = Paths.get("/home/sjo");
        try (Stream<Path> filesStream = Files.walk(path)) {
            List<Path> files = filesStream.collect(toList());
            for(Path file:files)
                System.out.println(file);
        }

    }
}
