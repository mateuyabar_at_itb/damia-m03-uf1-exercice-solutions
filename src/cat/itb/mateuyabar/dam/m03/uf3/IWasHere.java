package cat.itb.mateuyabar.dam.m03.uf3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;

import static java.nio.file.StandardOpenOption.*;

public class IWasHere {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        Path path = Path.of(homePath, "i_was_here.txt");
        String now = LocalDateTime.now().toString();
        String text = "I Was Here: "+now+"\n";
        Files.writeString(path, text, CREATE, WRITE, APPEND);
    }
}
