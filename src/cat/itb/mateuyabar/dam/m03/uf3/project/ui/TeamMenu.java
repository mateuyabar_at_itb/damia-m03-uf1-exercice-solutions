package cat.itb.mateuyabar.dam.m03.uf3.project.ui;

import cat.itb.mateuyabar.dam.m03.uf3.project.data.League;
import cat.itb.mateuyabar.dam.m03.uf3.project.data.Team;

import java.util.List;
import java.util.Scanner;

public class TeamMenu {
    public static void displayTeamsMenu(Scanner scanner, League league) {
        System.out.printf("display tems options");
        int operation = scanner.nextInt();
        switch (operation) {
            case 0:
                displayTeams(league);
            case 1:
                insertTeam(scanner, league);
                break;
            case 2:
                updateTeam(scanner, league);
            // TODO
        }
    }

    private static void displayTeams(League league) {
        List<Team> teams = league.getTeams();
        for(Team team : teams){
            System.out.println(team.getName());
        }
    }

    private static void insertTeam(Scanner scanner, League league) {
        Team team = readTeam(scanner);
        league.insert(team);
    }


    public static Team selectTeam(Scanner scanner, League league) {
        String code = scanner.next();
        return league.findByCode(code);
    }

    private static Team readTeam(Scanner scanner) {
        // TODO read values from scanner
        return new Team("", "");
    }

    private static void updateTeam(Scanner scanner, League league){
        Team team = selectTeam(scanner, league);
        updateTeam(scanner, team);
    }

    private static void updateTeam(Scanner scanner, Team team){
        System.out.println("What do you want to change");
        int option = scanner.nextInt();
        switch (option){
            case 0: //name
                updateTeamName(scanner, team);
            // TODO
        }
    }

    private static void updateTeamName(Scanner scanner, Team team) {
        // TODO ask user for new name and update
    }

}
