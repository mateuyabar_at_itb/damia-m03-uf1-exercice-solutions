package cat.itb.mateuyabar.dam.m03.uf3.project.ui;



import cat.itb.mateuyabar.dam.m03.uf3.project.data.League;

import java.util.Scanner;

/**
 * Displays main menu of the application
 */
public class MainMenu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        League league = new League();
        while(true) {
            System.out.printf("men");
            int operation = scanner.nextInt();
            switch (operation) {
                case 0:
                    TeamMenu.displayTeamsMenu(scanner, league);
                    break;
                case 3:
                    StatsMenu.displayStatsMenu(scanner, league);
                    break;
                case 1: //Load from file
                    League loadedLeague = StorageMenu.displayStorageMenu(scanner, league);
                    if(loadedLeague!=null)
                        league = loadedLeague;
                case 10: //exit
                    return;

            }
        }
    }


}
