package cat.itb.mateuyabar.dam.m03.uf3.project.ui;


import cat.itb.mateuyabar.dam.m03.uf3.project.data.League;
import cat.itb.mateuyabar.dam.m03.uf3.project.data.Team;

import java.util.Scanner;

/**
 * Displays stats menu of the application
 */
public class StatsMenu {
    public static void displayStatsMenu(Scanner scanner, League league) {
        System.out.printf("display tems options");
        int operation = scanner.nextInt();
        switch (operation) {
            case 1:
                displayTotalGoalsForTeam(scanner, league);
                break;
            case 2:
                // TODO
        }
    }

    private static void displayTotalGoalsForTeam(Scanner scanner, League league) {
        Team team  = TeamMenu.selectTeam(scanner, league);
        int goals = league.countTotalGoalsForTeam(team);
        System.out.println(goals);
    }


}
