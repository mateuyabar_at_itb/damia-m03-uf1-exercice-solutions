package cat.itb.mateuyabar.dam.m03.uf3;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileExists {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String stringPath = scanner.next();
        Path path = Paths.get(stringPath);
        boolean exists = Files.exists(path);
        System.out.println(exists);
    }
}
