package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {

    /**
     * Reads integers until -1
     * @param scanner
     * @return
     */
    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> list = new ArrayList<>();
        int value = scanner.nextInt();
        while(value!=-1) {
            list.add(value);
            value = scanner.nextInt();
        }
        return list;
    }

    public static List<Integer> readIntegerListFromFile(Path path) throws IOException {
        List<Integer> speeds = new ArrayList<>();
        Scanner fileScanner = new Scanner(path);
        while(fileScanner.hasNext()){
            int speed = fileScanner.nextInt();
            speeds.add(speed);
        }
        return speeds;
    }

    /**
     * Calculates the minimum value of a list
     * @param list where to serach
     * @return min value
     */
    public static int min(List<Integer> list){
        int min = list.get(0);
        for(int value: list){
            min = Math.min(min, value);
        }
        return min;
    }

    public static int max(List<Integer> list) {
        int max = list.get(0);
        for(int value: list){
            max = Math.max(max, value);
        }
        return max;
    }



    /**
     * Calculates the average of the list
     * @param list
     * @return average
     */
    public static double avg(List<Integer> list) {
        return (double)sum(list)/list.size();
    }

    public static int sum(List<Integer> list) {
        int sum = 0;
        for(int value:list){
            sum+=value;
        }
        return sum;
    }
}
