package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CovidApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> cases = CovidCalculations.readDailyCasesFromScanner(scanner);
        int totalCases = CovidCalculations.countTotalCases(cases);
        double averageCases = CovidCalculations.average(cases);
        List<Double> growRates = CovidCalculations.growthRates(cases);
        double lastGrowRate = growRates.get(growRates.size()-1);

        System.out.printf("Hi ha hagut %d casos en total, amb una mitjana de %.2f per dia.%n", totalCases, averageCases);
        System.out.printf("L'útlim creixement és de %.2f%n", lastGrowRate);
    }
}
