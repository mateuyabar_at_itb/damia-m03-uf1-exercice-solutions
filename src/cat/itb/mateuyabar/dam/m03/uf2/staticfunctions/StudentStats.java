package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class StudentStats {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> grades = IntegerLists.readIntegerList(scanner);
        int minGrade = IntegerLists.min(grades);
        int maxGrade = IntegerLists.max(grades);
        double avgGrade = IntegerLists.avg(grades);

        System.out.printf("Nota mínima:  %d%n", minGrade);
        System.out.printf("Nota màxima:  %d%n", maxGrade);
        System.out.printf("Nota mitjana: %.1f%n", avgGrade);
    }
}
