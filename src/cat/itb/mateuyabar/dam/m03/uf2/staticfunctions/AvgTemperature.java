package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class AvgTemperature {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> temperatures = IntegerLists.readIntegerList(scanner);
        double averageTemperature = IntegerLists.avg(temperatures);
        System.out.printf("%.2f", averageTemperature);
    }
}
