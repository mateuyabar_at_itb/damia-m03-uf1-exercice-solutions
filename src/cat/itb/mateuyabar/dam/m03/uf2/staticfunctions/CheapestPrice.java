package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CheapestPrice {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        int minPrice = IntegerLists.min(list);
        System.out.println(minPrice);
    }
}
