package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.util.Scanner;

public class PersonValidator {
    /**
     * Returns true if is a valid phone number (composed only by digits, spaces or +)
     * @param phone
     * @return true if valid
     */
    public static boolean isValidPhoneNumber(String phone){
        for(int i=0; i<phone.length(); ++i){
            char digit = phone.charAt(i);
            if(!Character.isDigit(digit) &&  digit!=' ' && digit!='+'){
                return false;
            }
        }
        return true;
    }

    /**
     * Returns true if is a valid person name (composed only characters and starts with upper case)
     * @param personName
     * @return true if valid
     */
    public static boolean isValidPersonName(String personName){
        char firstLetter = personName.charAt(0);
        if(!Character.isUpperCase(firstLetter))
            return false;

        for(int i=0; i<personName.length(); ++i){
            char letter = personName.charAt(i);
            if(!Character.isLetter(letter)){
                return false;
            }
        }

        return true;
    }

    /**
     * Returns true if is a valid dni (including correct letter)
     * @param dni
     * @return true if valid
     */
    public static boolean isValidDni(String dni){
        if(dni.length()!=9)
            return false;
        for(int i=0; i<8; ++i){
            char number = dni.charAt(i);
            if(!Character.isDigit(number)){
                return false;
            }
        }
        char letter = dni.charAt(8);
        if(!Character.isLetter(letter))
            return false;

        String dniStringNumber = dni.substring(0,8);
        int number = Integer.parseInt(dniStringNumber);
        char dniCalcLetter = calculateLetterFromDni(number);
        if(letter!=dniCalcLetter)
            return false;

        return true;
    }

    private static char calculateLetterFromDni(int number) {
        char[] letters = {'T','R','W','A','G','M',
                'Y','F','P','D','X','B','N','J',
                'Z','S','Q','V','H','L','C','K','E'};

        int position = number%23;
        char letter = letters[position];
        return letter;
    }

    /**
     * Returns true if is a valid postalCode
     * @param postalCode
     * @return true if valid
     */
    public static boolean isValidPostalcode(String postalCode){
        // TODO
        return true;
    }
}