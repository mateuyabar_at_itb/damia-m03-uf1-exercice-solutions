package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

public class Rectangle {
    double height;
    double width;

    public Rectangle(double height, double width){
        this.height = height;
        this.width = width;
    }

    public double getHeight(){
        return height;
    }

    public double getWidth(){
        return width;
    }

    public void setHeight(double height){
        this.height = height;
    }
}
