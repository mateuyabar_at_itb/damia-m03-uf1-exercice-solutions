package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerListsReaderSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        System.out.println(list);
    }

}
