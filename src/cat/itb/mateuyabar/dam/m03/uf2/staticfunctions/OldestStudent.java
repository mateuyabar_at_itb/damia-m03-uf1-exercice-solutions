package cat.itb.mateuyabar.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class OldestStudent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> ages = IntegerLists.readIntegerList(scanner);
        int maxAge = IntegerLists.max(ages);
        System.out.println(maxAge);
    }
}
