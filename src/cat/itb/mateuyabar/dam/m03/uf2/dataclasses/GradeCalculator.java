package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GradeCalculator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<StudentGrades> students = readStudents(scanner);
        printStudents(students);
    }

    private static void printStudents(List<StudentGrades> students) {
        for(StudentGrades student: students){
            printStudent(student);
        }
    }

    private static void printStudent(StudentGrades student) {
        String name = student.getName();
        double averageGrade = student.getAvgGrade();
        System.out.printf("%s: %.2f", name, averageGrade);
    }


    private static List<StudentGrades> readStudents(Scanner scanner) {
        List<StudentGrades> result = new ArrayList<>();
        int size = scanner.nextInt();
        for(int i=0; i<size; ++i){
            StudentGrades studentGrades = readStudent(scanner);
            result.add(studentGrades);
        }
        return result;
    }

    private static StudentGrades readStudent(Scanner scanner) {
        String name = scanner.next();
        double notaExercicis = scanner.nextDouble();
        double notaExamen = scanner.nextDouble();
        double notaProjecte = scanner.nextDouble();
        return new StudentGrades(name, notaExercicis, notaExamen, notaProjecte);
    }
}
