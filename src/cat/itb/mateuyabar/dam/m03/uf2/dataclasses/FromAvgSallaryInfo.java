package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FromAvgSallaryInfo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Employee> employees = readEmployees(scanner);
        List<Employee> underAvgSalary = filterUnderAvgSalary(employees);
        printEmployees(underAvgSalary);
        printEmployees(employees);
    }

    private static void printEmployees(List<Employee> employees) {
        for(Employee employee: employees){
            System.out.println(employee.getName());
        }
    }

    private static List<Employee> filterUnderAvgSalary(List<Employee> employees) {
        double averageSalary = calculateAverageSalary(employees);
        return filterByLowerSalary(employees, averageSalary);
    }

    private static List<Employee> filterByLowerSalary(List<Employee> employees, double salary) {
        List<Employee> filtered = new ArrayList<>();
        for(Employee employee: employees){
            if(employee.getSalary()<salary){
                filtered.add(employee);
            }
        }
        return filtered;
    }

    private static double calculateAverageSalary(List<Employee> employees) {
        double sum = 0;
        for(Employee employee : employees){
            sum+= employee.getSalary();
        }
        return sum/employees.size();
    }

    private static List<Employee> readEmployees(Scanner scanner) {
        int size = scanner.nextInt();
        List<Employee> result = new ArrayList<>();
        for(int i=0; i<size; ++i){
            Employee employee = readEmployee(scanner);
            result.add(employee);
        }
        return result;
    }

    private static Employee readEmployee(Scanner scanner) {
        double salary = scanner.nextInt();
        String name = scanner.nextLine();
        return new Employee(salary, name);
    }
}
