package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.Scanner;

public class School {
    String name;
    String address;
    String postalCode;
    String city;

    public School(String name, String address, String postalCode, String city) {
        this.name = name;
        this.address = address;
        this.postalCode = postalCode;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    @Override
    public String toString() {
        return String.format("%s%n"
            +"adreça: %s%n"
            +"codipostal: %s%n"
            + "ciutat: %s",
                name, address, postalCode, city);

    }

    public static School readSchool(Scanner scanner) {
        String name = scanner.nextLine();
        String address = scanner.nextLine();
        String postalCode = scanner.nextLine();
        String city = scanner.nextLine();
        return new School(name, address, postalCode, city);
    }
}
