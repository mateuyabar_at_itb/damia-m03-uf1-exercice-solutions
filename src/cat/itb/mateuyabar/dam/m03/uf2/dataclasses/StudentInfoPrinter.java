package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StudentInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<School> schools = readSchools(scanner);
        List<Student> students = readStudents(scanner, schools);
        printStudents(students);
    }

    private static void printStudents(List<Student> students) {
        for(Student student: students){
            printStudent(student);
            System.out.println("--------------------");
        }
    }

    private static void printStudent(Student student) {
        System.out.println(student);
    }

    private static List<Student> readStudents(Scanner scanner, List<School> schools) {
        List<Student> students = new ArrayList<>();
        int size = scanner.nextInt();
        scanner.nextLine();
        for(int i = 0; i<size; ++i){
            Student student = readStudent(scanner, schools);
            students.add(student);
        }
        return students;
    }

    private static Student readStudent(Scanner scanner, List<School> schools) {
        int schoolNumber = scanner.nextInt();
        scanner.nextLine();
        String name = scanner.nextLine();
        School school = schools.get(schoolNumber);
        return new Student(name, school);
    }

    private static List<School> readSchools(Scanner scanner) {
        List<School> schools = new ArrayList<>();
        int size = scanner.nextInt();
        scanner.nextLine();
        for(int i = 0; i<size; ++i){
            School school = School.readSchool(scanner);
            schools.add(school);
        }
        return schools;
    }
}
