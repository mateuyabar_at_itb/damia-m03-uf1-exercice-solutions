package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

public class Student {
    String name;
    School school;

    public Student(String name, School school) {
        this.name = name;
        this.school = school;
    }

    public String getName() {
        return name;
    }

    public School getSchool() {
        return school;
    }

    public String toString(){
        return school + "\n"+ name;
    }
}
