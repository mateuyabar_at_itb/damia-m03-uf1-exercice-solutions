package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

public class RectangleApp {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(2,5.4);
        double width = rectangle.getWidth();
        System.out.println(width);

        rectangle.setWidth(4);
        System.out.println(rectangle.getWidth());
    }
}
