package cat.itb.mateuyabar.dam.m03.uf2.dataclasses;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MultiProductInfoPrinter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Product> products = readProducts(scanner);
        printProducts(products);
    }

    private static void printProducts(List<Product> products) {
        for(Product product : products){
            Product.printProduct(product);
        }
    }

    private static List<Product> readProducts(Scanner scanner) {
        int productNumber = scanner.nextInt();
        scanner.nextLine();
        List<Product> products = new ArrayList<>();
        for(int i=0; i<productNumber; ++i){
            Product product = Product.readProduct(scanner);
            products.add(product);
        }
        return products;
    }
}
