package cat.itb.mateuyabar.dam.m03.uf2.recursivity;

public class MultiplicationRecursive {

    public static int multiply(int n, int m){
        if(m==0)
            return 0;
        return n + multiply(n, m-1);//n*(m-1);
    }
}
