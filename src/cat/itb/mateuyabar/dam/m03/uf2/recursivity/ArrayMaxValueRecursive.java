package cat.itb.mateuyabar.dam.m03.uf2.recursivity;

import java.util.List;

public class ArrayMaxValueRecursive {

    public static int getMax(List<Integer> values){
        return getMax(values, 0);
    }
    public static int getMax(List<Integer> values, int i){
        if(i==values.size()-1){
            return values.get(i);
        }
        return Math.max(values.get(i), getMax(values, i+1));

    }
}
