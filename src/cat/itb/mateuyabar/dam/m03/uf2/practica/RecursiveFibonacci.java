package cat.itb.mateuyabar.dam.m03.uf2.practica;

public class RecursiveFibonacci {

    public int fibonacci(int n){
        if(n==0)
            return 0;
        if(n==1)
            return 1;
        return fibonacci(n-1)+fibonacci(n-2);
    }
}
