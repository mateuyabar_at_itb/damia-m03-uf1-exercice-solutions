package cat.itb.mateuyabar.dam.m03.uf2.practica;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HighPeaks {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Peak> peaks = readPeaks(scanner);
        print(peaks);
    }

    private static void print(List<Peak> peaks) {
        pirntSpearatorLine();
        System.out.println("--- Cims aconseguits ---");
        pirntSpearatorLine();
        printPeakList(peaks);
        pirntSpearatorLine();
        printResumee(peaks);

    }

    private static void printResumee(List<Peak> peaks) {
        printCount(peaks);
        printHighest(peaks);
        printShortest(peaks);
        printFastest(peaks);
    }

    private static void printFastest(List<Peak> peaks) {
        Peak peak = getFastest(peaks);
        System.out.printf("Cim més veloç: %s %.2fkm/hora", peak.getName(), peak.getSpeed());
    }

    private static Peak getFastest(List<Peak> peaks) {
        Peak result= peaks.get(0);
        for(Peak peak: peaks){
            if(peak.getSpeed()>result.getSpeed())
                result = peak;
        }
        return result;
    }

    private static void printShortest(List<Peak> peaks) {
        Peak peak = getShortest(peaks);
        System.out.printf("Cim més ràpid: %s (%s)", peak.getName(), peak.getDuration());
    }

    private static Peak getShortest(List<Peak> peaks) {
        Peak result= peaks.get(0);
        for(Peak peak: peaks){
            if(peak.getDinstance()<result.getDinstance())
                result = peak;
        }
        return result;
    }


    private static void printHighest(List<Peak> peaks) {
        Peak peak = getHighest(peaks);
        System.out.printf("Cim més alt: %s (%dm)", peak.getName(), peak.getHight());
    }

    private static Peak getHighest(List<Peak> peaks) {
        Peak result= peaks.get(0);
        for(Peak peak: peaks){
            if(peak.getHight()>result.getHight())
                result = peak;
        }
        return result;
    }

    private static void printCount(List<Peak> peaks) {
        System.out.printf("N. cims: %d", peaks.size());
    }

    private static void printPeakList(List<Peak> peaks) {
        for(Peak peak : peaks)
            printpeak(peak);
    }

    private static void printpeak(Peak peak) {
        System.out.printf("%s - %s (%dm) - Temps: %s", peak.getName(), peak.getCountry(), peak.getHight(), peak.getDuration());
    }

    private static void pirntSpearatorLine() {
        System.out.println("------------------------");
    }

    private static List<Peak> readPeaks(Scanner scanner) {
        List<Peak> result = new ArrayList<>();
        int count = scanner.nextInt();
        scanner.nextLine();
        for(int i =0; i<count; ++i){
            Peak peak = readPeak(scanner);
            result.add(peak);
        }
        return result;
    }

    private static Peak readPeak(Scanner scanner) {
        String name = scanner.nextLine();
        int hight = scanner.nextInt();
        scanner.nextLine();
        String country = scanner.nextLine();;
        int dinstance= scanner.nextInt();
        int minutes = scanner.nextInt();
        return new Peak(name, hight, country, dinstance, minutes);
    }
}
