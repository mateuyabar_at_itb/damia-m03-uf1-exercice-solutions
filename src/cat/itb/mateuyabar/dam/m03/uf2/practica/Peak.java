package cat.itb.mateuyabar.dam.m03.uf2.practica;

public class Peak {
    String name;
    int hight;
    String country;
    int dinstance;
    int minutes;

    public Peak(String name, int hight, String country, int dinstance, int minutes) {
        this.name = name;
        this.hight = hight;
        this.country = country;
        this.dinstance = dinstance;
        this.minutes = minutes;
    }

    public String getName() {
        return name;
    }

    public int getHight() {
        return hight;
    }

    public String getCountry() {
        return country;
    }

    public int getDinstance() {
        return dinstance;
    }

    public int getMinutes() {
        return minutes;
    }

    public int getHours() {
        return minutes/60;
    }

    public int getMinutsWithHours() {
        return minutes%60;
    }

    public String getDuration() {
        return String.format("%d:%02d", getHight(), getMinutsWithHours());
    }

    public double getSpeed() {
        return (dinstance/1000.0)/(minutes/60.0);
    }
}
