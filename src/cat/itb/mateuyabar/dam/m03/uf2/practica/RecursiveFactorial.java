package cat.itb.mateuyabar.dam.m03.uf2.practica;

import java.util.Scanner;

public class RecursiveFactorial {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();
        int factorial = factorial(value);
        System.out.println(factorial);
    }

    public static int factorial(int n){
        if(n==0)
            return 1;
        return factorial(n-1)*n;
    }
}
