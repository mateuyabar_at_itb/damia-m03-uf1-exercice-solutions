package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RectangleSize {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Rectangle> rectangleList = readRectangles(scanner);
        printRectangles(rectangleList);
    }

    public static void printRectangles(List<Rectangle> rectangleList) {
        for(Rectangle rectangle: rectangleList){
            printRectangle(rectangle);
        }
    }

    private static void printRectangle(Rectangle rectangle) {
        saveRectangle(rectangle, System.out);
    }

    private static void saveRectangle(Rectangle rectangle, PrintStream printStream) {
        printStream.println(rectangle);
    }

    public static List<Rectangle> readRectangles(Scanner scanner) {
        List<Rectangle> result = new ArrayList<>();
        int size = scanner.nextInt();
        for(int i = 0; i<size; ++i){
            Rectangle rectangle = readRectangle(scanner);
            result.add(rectangle);
        }
        return result;
    }

    private static Rectangle readRectangle(Scanner scanner) {
        double height = scanner.nextDouble();
        double width = scanner.nextDouble();
        return new Rectangle(height, width);
    }
}
