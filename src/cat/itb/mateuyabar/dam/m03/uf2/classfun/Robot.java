package cat.itb.mateuyabar.dam.m03.uf2.classfun;

public class Robot {
    double x;
    double y;
    double speed;

    public Robot(double x, double y, double speed) {
        this.x = x;
        this.y = y;
        this.speed = speed;
    }

    public void moveUp(){
        y+=speed;
    }

    public void moveDown(){
        y-=speed;
    }
    public void moveRight(){
        x+=speed;
    }
    public void moveLeft(){
        x-=speed;
    }

    public void accelerate(){
        if(speed<10)
            speed+=0.5;
    }

    public void slowDown(){
        if(speed>0)
            speed-=0.5;
    }

    public void printSpeed(){
        System.out.println(speed);
    }

    public void printPosition(){
        System.out.printf("La posició del robot és (%.1f, %.1f)%n", x, y);
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getSpeed() {
        return speed;
    }
}
