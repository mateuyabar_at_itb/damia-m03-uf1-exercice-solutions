package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import java.util.Scanner;

public class BasicRobot {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Robot robot = new Robot(0,0,1);
        String opration = scanner.next();
        while(!opration.equals("END")){
            doOperation(robot, opration);
            opration = scanner.next();
        }
    }

    private static void doOperation(Robot robot, String opration) {
        switch (opration) {
            case "DALT":
                robot.moveUp();
                break;
            case "BAIX":
                robot.moveDown();
                break;
            case "DRETA":
                robot.moveRight();
                break;
            case "ESQUERRA":
                robot.moveLeft();
                break;
            case "ACCELERAR":
                robot.accelerate();
                break;
            case "DISMINUIR":
                robot.slowDown();
                break;
            case "POSICIO":
                robot.printPosition();
                break;
            case "VELOCITAT":
                robot.printSpeed();
                break;
        }
    }
}
