package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class PeopleCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> peopleCount = IntegerLists.readIntegerList(scanner);
        int people = IntegerLists.sum(peopleCount);
        System.out.println(people);
    }
}
