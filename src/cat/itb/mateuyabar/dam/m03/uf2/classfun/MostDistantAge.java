package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import cat.itb.mateuyabar.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MostDistantAge {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> ages = IntegerLists.readIntegerList(scanner);
        int distance = getDistance(ages);
        System.out.println(distance);
    }

    private static int getDistance(List<Integer> ages) {
        int max = IntegerLists.max(ages);
        int min = IntegerLists.min(ages);
        return max-min;
    }
}
