package cat.itb.mateuyabar.dam.m03.uf2.classfun;

public class ShoppingItem {
    int amount;
    String name;
    double price;

    public ShoppingItem(int amount, String name, double price) {
        this.amount = amount;
        this.name = name;
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public double getTotal(){
        return amount*price;
    }

    public String toString(){
        return String.format("%d %s (%.2f€) - %.2f€", amount, name, price, getTotal());
    }
}
