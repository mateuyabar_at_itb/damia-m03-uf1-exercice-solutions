package cat.itb.mateuyabar.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BuyList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<ShoppingItem> shoppingItemList = readShoppingItems(scanner);
        printShoppingDetails(shoppingItemList);
    }

    private static void printShoppingDetails(List<ShoppingItem> shoppingItemList) {
        System.out.println("-------- Compra --------");
        printItems(shoppingItemList);
        System.out.println("-------------------------");
        printResumee(shoppingItemList);
        System.out.println("-------------------------");
    }

    private static void printResumee(List<ShoppingItem> shoppingItemList) {
        double total = getTotal(shoppingItemList);
        System.out.printf("Total: %.2f€%n", total);
    }

    private static double getTotal(List<ShoppingItem> shoppingItemList) {
        double total = 0;
        for(ShoppingItem shoppingItem:shoppingItemList){
            total+=shoppingItem.getTotal();
        }
        return total;
    }

    private static void printItems(List<ShoppingItem> shoppingItemList) {
        for(ShoppingItem item: shoppingItemList){
            printItem(item);
        }
    }

    private static void printItem(ShoppingItem item) {
        System.out.println(item);
    }

    private static List<ShoppingItem> readShoppingItems(Scanner scanner) {
        List<ShoppingItem> list = new ArrayList<>();
        int size = scanner.nextInt();
        for(int i=0; i<size; ++i){
            ShoppingItem item = readShoppingItem(scanner);
            list.add(item);
        }
        return list;
    }

    private static ShoppingItem readShoppingItem(Scanner scanner) {
        int amount = scanner.nextInt();
        String name = scanner.next();
        double price = scanner.nextDouble();
        return new ShoppingItem(amount, name, price);
    }
}
