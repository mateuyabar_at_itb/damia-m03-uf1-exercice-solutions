package cat.itb.mateuyabar.dam.m03.uf2.classfun;

public class Rectangle {
    double height;
    double width;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public double getWidth() {
        return width;
    }

    public double getArea() {
        return height*width;
    }

    @Override
    public String toString() {
        return String.format("Un rectangle de %.1f x %.1f té %.1f d'area.", width, height, getArea());
    }

}
