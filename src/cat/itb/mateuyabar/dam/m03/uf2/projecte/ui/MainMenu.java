package cat.itb.mateuyabar.dam.m03.uf2.projecte.ui;





import cat.itb.mateuyabar.dam.m03.uf2.projecte.data.League;

import java.util.Scanner;

/**
 * Displays main menu of the application
 */
public class MainMenu {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        League league = new League();
        boolean exit = MainMenu.displayMenu(scanner, league);
        while(!exit)
            exit = MainMenu.displayMenu(scanner, league);
    }

    public static boolean displayMenu(Scanner scanner, League league){
        System.out.printf("men");
        int operation = scanner.nextInt();
        switch (operation) {
            case 0:
                TeamMenu.displayTeamsMenu(scanner, league);
                return false;
            case 3:
                StatsMenu.displayStatsMenu(scanner, league);
            case 6: //exit
                return true;
            default:
                return false;
        }
    }


}
