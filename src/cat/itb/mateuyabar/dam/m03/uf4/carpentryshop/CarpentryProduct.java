package cat.itb.mateuyabar.dam.m03.uf4.carpentryshop;

public abstract class CarpentryProduct {
    double unitPrice;

    public CarpentryProduct(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotalPrice(){
        return unitPrice* getAmount();
    }

    protected abstract double getAmount();
}
