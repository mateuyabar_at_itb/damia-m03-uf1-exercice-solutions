package cat.itb.mateuyabar.dam.m03.uf4.carpentryshop;

public class Board extends CarpentryProduct{
    double height;
    double width;

    public Board(double unitPrice, double height, double width) {
        super(unitPrice);
        this.height = height;
        this.width = width;
    }

    @Override
    protected double getAmount() {
        return height*width;
    }
}
