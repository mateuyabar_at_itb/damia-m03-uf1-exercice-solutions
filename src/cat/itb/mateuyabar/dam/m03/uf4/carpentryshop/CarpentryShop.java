package cat.itb.mateuyabar.dam.m03.uf4.carpentryshop;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CarpentryShop {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);
        List<CarpentryProduct> products = readProducts(scanner);
        double total = calculateTotal(products);
        System.out.printf("El preu total és: %.2f€%n", total);
    }

    private static double calculateTotal(List<CarpentryProduct> products) {
        double price = 0;
        for(CarpentryProduct carpentryProduct: products){
            price+= carpentryProduct.getTotalPrice();
        }
        return price;
    }

    private static List<CarpentryProduct> readProducts(Scanner scanner) {
        List<CarpentryProduct> products = new ArrayList<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            CarpentryProduct product = readProduct(scanner);
            products.add(product);
        }
        return products;
    }

    private static CarpentryProduct readProduct(Scanner scanner) {
        String type = scanner.next();
        switch (type){
            case "Taulell":
                return readBoard(scanner);
            case "Llistó":
                return readStrip(scanner);
        }
        System.out.println("Producte no correcte");
        return readProduct(scanner);
    }

    private static CarpentryProduct readStrip(Scanner scanner) {
        double price = scanner.nextDouble();
        double length = scanner.nextDouble();
        return new Strip(price, length);
    }

    private static CarpentryProduct readBoard(Scanner scanner) {
        double price = scanner.nextDouble();
        double height = scanner.nextDouble();
        double width = scanner.nextDouble();
        return new Board(price, height, width);
    }
}
