package cat.itb.mateuyabar.dam.m03.uf4;

import java.util.List;

public interface WaterPlant {
    List<Double> getHumidityRecord();
    void startWatterSystem();
}
