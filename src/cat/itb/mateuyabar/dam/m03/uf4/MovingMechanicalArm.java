package cat.itb.mateuyabar.dam.m03.uf4;

public class MovingMechanicalArm extends MechanicalArm{
    double position;

    public void move(double movement){
        if(turnedOn)
            position = Math.max(0, movement+position);
    }

    @Override
    public String toString() {
        return "MovingMechanicalArm{" +
                "turnedOn=" + turnedOn +
                ", altitude=" + altitude +
                ", openAngle=" + openAngle +
                ", position=" + position +
                '}';
    }
}
