package cat.itb.mateuyabar.dam.m03.uf4;

import java.util.List;

public class WaterPlantControler {
    WaterPlant waterPlant;

    public WaterPlantControler(WaterPlant waterPlant) {
        this.waterPlant = waterPlant;
    }

    public void waterIfNeeded(){
        if(waterIsNeed())
            waterPlant.startWatterSystem();
    }

    private boolean waterIsNeed() {
        List<Double> values = waterPlant.getHumidityRecord();
        double average = average(values);
        return average<2;
    }

    private double average(List<Double> values) {
        return sum(values)/values.size();
    }

    private double sum(List<Double> values) {
        double sum= 0;
        for(Double value:values)
            sum+=value;
        return sum;
    }
}
