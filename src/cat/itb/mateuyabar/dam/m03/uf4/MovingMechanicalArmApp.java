package cat.itb.mateuyabar.dam.m03.uf4;

public class MovingMechanicalArmApp {
    public static void main(String[] args) {
        MovingMechanicalArm movingMechanicalArm = new MovingMechanicalArm();

        movingMechanicalArm.setTurnedOn(true);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.updateAltitude(3);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.move(4.5);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.updateAngle(180);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.updateAltitude(-3);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.updateAngle(-180);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.updateAltitude(3);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.move(-4.5);
        System.out.println(movingMechanicalArm);

        movingMechanicalArm.setTurnedOn(false);
        System.out.println(movingMechanicalArm);


    }
}
