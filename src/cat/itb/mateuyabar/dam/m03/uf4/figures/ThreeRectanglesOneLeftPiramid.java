package cat.itb.mateuyabar.dam.m03.uf4.figures;

public class ThreeRectanglesOneLeftPiramid {
    public static void main(String[] args) {
        // color: RED, llargada: 4, amplada: 5
        RectangleFigure rectangleFigure = new RectangleFigure(ConsoleColors.RED, 4,5);
        rectangleFigure.paint(System.out);
        LeftPiramidFigure leftPiramidFigure = new LeftPiramidFigure(ConsoleColors.YELLOW, 3);
        leftPiramidFigure.paint(System.out);
        rectangleFigure = new RectangleFigure(ConsoleColors.GREEN, 3,5);
        rectangleFigure.paint(System.out);
    }
}
