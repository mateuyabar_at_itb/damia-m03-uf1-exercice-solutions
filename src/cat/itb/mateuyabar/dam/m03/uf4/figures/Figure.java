package cat.itb.mateuyabar.dam.m03.uf4.figures;

import java.io.PrintStream;

public abstract class Figure {
    String color;

    public Figure(String color) {
        this.color = color;
    }

    public void changeConsoleColor(PrintStream printStream){
        printStream.print(color);
    }

    public void resetConsoleColor(PrintStream printStream){
        printStream.print(ConsoleColors.RESET);
    }

    public void paint(PrintStream printStream){
        changeConsoleColor(printStream);
        paintDots(printStream);
        resetConsoleColor(printStream);
    }

    protected abstract void paintDots(PrintStream printStream);
}
