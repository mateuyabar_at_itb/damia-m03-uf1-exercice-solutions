package cat.itb.mateuyabar.dam.m03.uf4.teammoto;

import java.util.ArrayList;
import java.util.List;

public class TeamMotto {
    public static void main(String[] args) {
        List<Team> teamList = new ArrayList<>();
        teamList.add(new Team("Mosques", "Bzzzanyarem"));
        teamList.add(new Team("Dragons", "Grooarg"));
        teamList.add(new Team("Abelles", "Piquem Fort"));
        shoutMottos(teamList);
    }

    public static void shoutMottos(List<Team> teams){
        for(Team team: teams)
            team.shoutMotto();
    }
}
