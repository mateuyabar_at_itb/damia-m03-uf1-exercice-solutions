package cat.itb.mateuyabar.dam.m03.uf4.teammoto;

public class Team {
    String name;
    String motto;

    public Team(String name, String motto) {
        this.name = name;
        this.motto = motto;
    }

    public void shoutMotto(){
        System.out.println(motto);
    }
}
