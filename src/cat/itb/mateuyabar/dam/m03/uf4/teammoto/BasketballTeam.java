package cat.itb.mateuyabar.dam.m03.uf4.teammoto;

public class BasketballTeam extends Team {
    public BasketballTeam(String name, String motto) {
        super(name, motto);
    }

    @Override
    public void shoutMotto() {
        System.out.println("1,2,3 "+motto);
    }
}
