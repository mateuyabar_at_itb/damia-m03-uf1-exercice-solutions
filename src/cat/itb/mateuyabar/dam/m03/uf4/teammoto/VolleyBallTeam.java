package cat.itb.mateuyabar.dam.m03.uf4.teammoto;

public class VolleyBallTeam extends Team{
    String color;

    public VolleyBallTeam(String name, String motto, String color) {
        super(name, motto);
        this.color = color;
    }

    @Override
    public void shoutMotto() {
        System.out.println(motto+" "+color);
    }
}
