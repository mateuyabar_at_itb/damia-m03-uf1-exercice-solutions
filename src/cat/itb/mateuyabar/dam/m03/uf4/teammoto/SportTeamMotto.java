package cat.itb.mateuyabar.dam.m03.uf4.teammoto;

import java.util.ArrayList;
import java.util.List;

public class SportTeamMotto {
    public static void main(String[] args) {
        List<Team> teamList = new ArrayList<>();
        teamList.add(new BasketballTeam("Mosques", "Bzzzanyarem♜♜♜♜♜♜"));
        teamList.add(new VolleyBallTeam("Dragons", "Grooarg", "verd"));
        teamList.add(new GolfTeam("Abelles", "Piquem Fort", "Ot Pi"));
        teamList.add(new Team("ADSdsa", "FASdsada"));
        shoutMottos(teamList);
    }

    public static void shoutMottos(List<Team> teams){
        for(Team team: teams)
            team.shoutMotto();
    }
}
