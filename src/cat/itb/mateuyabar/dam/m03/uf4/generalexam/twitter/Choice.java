package cat.itb.mateuyabar.dam.m03.uf4.generalexam.twitter;

public class Choice{
    private String text;
    private int votes;

    public Choice(String text) {
        this.text = text;
        this.votes = 0;
    }

    public String getText() {
        return text;
    }

    public int getVotes() {
        return votes;
    }

    public void vote(){
        this.votes++;
    }
}
