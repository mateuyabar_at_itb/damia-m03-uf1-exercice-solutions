package cat.itb.mateuyabar.dam.m03.uf4.generalexam.twitter;

public class Tweet {
    String user;
    String date;
    String text;

    public Tweet(String user, String date, String text) {
        this.user = user;
        this.date = date;
        this.text = text;
    }

    public String getUser() {
        return user;
    }
    public String getDate() {
        return date;
    }
    public String getText() {
        return text;
    }

    public void print(){
        System.out.printf("@%s · %s\n", user, date);
        System.out.println(text);
        System.out.println();
    }
}
