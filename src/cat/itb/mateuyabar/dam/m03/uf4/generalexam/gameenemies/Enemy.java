package cat.itb.mateuyabar.dam.m03.uf4.generalexam.gameenemies;

public abstract class Enemy {
    String name;
    int livePoints;

    public Enemy(String name, int livePoints) {
        this.name = name;
        this.livePoints = livePoints;
    }

    public void attack(int strength){
        if(livePoints==0) {
            System.out.printf("L'enemic %s ja està mort%n", name);
            return;
        }
        resolveAttack(strength);
        System.out.printf("L'enemic %s té %d punts de vida després d'un atac de força %d%n", name, livePoints, strength);
    }

    protected abstract void resolveAttack(int strength);

    public void subtractLivePoints(int points){
        livePoints = Math.max(0, livePoints-points);
    }

    @Override
    public String toString() {
        return "Enemy{" +
                "name='" + name + '\'' +
                ", livePoints=" + livePoints +
                '}';
    }
}
