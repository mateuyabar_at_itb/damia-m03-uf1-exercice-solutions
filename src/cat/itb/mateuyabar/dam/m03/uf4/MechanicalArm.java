package cat.itb.mateuyabar.dam.m03.uf4;

public class MechanicalArm {
    boolean turnedOn;
    double altitude;
    double openAngle;


    public void setTurnedOn(boolean turnedOn) {
        this.turnedOn = turnedOn;
    }

    public void updateAltitude(double altitudeChange){
        if(turnedOn)
            altitude = Math.max(0,altitude+altitudeChange);
    }

    public void updateAngle(double angleChange){
        if(turnedOn) {
            openAngle = Math.min(360,Math.max(0,openAngle+angleChange));
        }
    }

    @Override
    public String toString() {
        return "MechanicalArm{" +
                "turnedOn=" + turnedOn +
                ", altitude=" + altitude +
                ", openAngle=" + openAngle +
                '}';
    }
}
