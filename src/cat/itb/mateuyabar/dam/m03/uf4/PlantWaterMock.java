package cat.itb.mateuyabar.dam.m03.uf4;

import java.util.ArrayList;
import java.util.List;

public class PlantWaterMock implements WaterPlant{
    @Override
    public List<Double> getHumidityRecord() {
        List<Double> list = new ArrayList<>();
        list.add(1.5);
        list.add(1.5);
        list.add(1.7);
        list.add(1.8);
        list.add(1.9);
        list.add(1.5);
        return list;
    }

    @Override
    public void startWatterSystem() {
        System.out.println("WATER!!!");
    }
}
