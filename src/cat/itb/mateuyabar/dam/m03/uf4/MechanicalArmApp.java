package cat.itb.mateuyabar.dam.m03.uf4;

public class MechanicalArmApp {
    public static void main(String[] args) {
        MechanicalArm mechanicalArm = new MechanicalArm();

        mechanicalArm.setTurnedOn(true);
        System.out.println(mechanicalArm);

        mechanicalArm.updateAltitude(3);
        System.out.println(mechanicalArm);

        mechanicalArm.updateAngle(180);
        System.out.println(mechanicalArm);

        mechanicalArm.updateAltitude(-3);
        System.out.println(mechanicalArm);

        mechanicalArm.updateAngle(-180);
        System.out.println(mechanicalArm);

        mechanicalArm.updateAltitude(3);
        System.out.println(mechanicalArm);

        mechanicalArm.setTurnedOn(false);
        System.out.println(mechanicalArm);



    }
}
