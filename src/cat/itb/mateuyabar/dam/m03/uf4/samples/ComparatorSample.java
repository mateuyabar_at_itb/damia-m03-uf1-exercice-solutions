package cat.itb.mateuyabar.dam.m03.uf4.samples;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorSample {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(5,8,4,7,6,7,8,100,8548,848,48,468,64,685,468,647,876);
        Collections.sort(list, (i1, i2) -> i2-i1);
        System.out.println(list);
    }
}
