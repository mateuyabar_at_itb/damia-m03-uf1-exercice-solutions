package cat.itb.mateuyabar.dam.m03.uf4.samples;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AreaSum {
    public static void main(String[] args) {
        List<Polygon> figures = new LinkedList<>();

        double totalArea = sumAreas(figures);
    }

    private static double sumAreas(List<Polygon> figures) {
        double total = 0;
        for(Polygon polygon: figures){
            total+=polygon.getArea();
        }
        return total;
    }
}
