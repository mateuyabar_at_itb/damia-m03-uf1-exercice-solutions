package cat.itb.mateuyabar.dam.m03.uf4.samples;



import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ComparableSample {
    public static void main(String[] args) {
        List<Rectangle> values = new ArrayList<>();
        values.add(new Rectangle(5,4));
        values.add(new Rectangle(5,40));
        values.add(new Rectangle(53,4));
        values.add(new Rectangle(543,4432));
        values.add(new Rectangle(1,2));

        Collections.sort(values);
        Collections.sort(values, (r1, r2) -> {
            return (int) (r1.height-r2.height);
        });
        System.out.println(values);
    }
}
