package cat.itb.mateuyabar.dam.m03.uf4.samples;

public enum DayOfWeek {
    SUNDAY, MONDAY, TUESDAY, WEDNESDAY,
    THURSDAY, FRIDAY, SATURDAY
}
