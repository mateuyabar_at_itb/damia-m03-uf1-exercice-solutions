package cat.itb.mateuyabar.dam.m03.uf4.samples;

public class Circle implements Polygon{
    double radius;


    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea(){
        return Math.PI*radius*radius;
    }
}
