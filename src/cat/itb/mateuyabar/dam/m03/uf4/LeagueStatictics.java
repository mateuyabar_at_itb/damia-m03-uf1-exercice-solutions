package cat.itb.mateuyabar.dam.m03.uf4;

import cat.itb.mateuyabar.dam.m03.uf2.projecte.data.League;
import cat.itb.mateuyabar.dam.m03.uf2.projecte.data.Team;

import java.util.List;

public class LeagueStatictics {
    League league;

    public LeagueStatictics(League league) {
        this.league = league;
    }

    public int countGoals(Team team){
        return league.getTeams().size();
    }
    public List<Team> classification(){
        return league.getTeams();
    }
}