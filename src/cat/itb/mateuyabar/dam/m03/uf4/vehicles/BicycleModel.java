package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

public class BicycleModel extends VehicleModel{
    int grears;

    public BicycleModel(String name, VehicleBrand vehicleBrand, int grears) {
        super(name, vehicleBrand);
        this.grears = grears;
    }

    @Override
    public String toString() {
        return "BicycleModel{" +
                "grears=" + grears +
                ", name='" + name + '\'' +
                ", vehicleBrand=" + vehicleBrand +
                '}';
    }
}
