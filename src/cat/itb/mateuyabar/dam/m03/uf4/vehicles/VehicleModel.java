package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

public class VehicleModel implements Comparable<VehicleModel>{
    String name;
    VehicleBrand vehicleBrand;

    public VehicleModel(String name, VehicleBrand vehicleBrand) {
        this.name = name;
        this.vehicleBrand = vehicleBrand;
    }

    @Override
    public String toString() {
        return "VehicleModel{" +
                "name='" + name + '\'' +
                ", vehicleBrand=" + vehicleBrand +
                '}';
    }

    @Override
    public int compareTo(VehicleModel vehicleModel) {
        return this.name.compareTo(vehicleModel.name);
    }
}
