package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VehicleComparableApp {
    public static void main(String[] args) {
        VehicleBrand brand = new VehicleBrand("dsa", "das");
        List<VehicleModel> vehicleModels = new ArrayList<>();
        vehicleModels.add(new ScooterModel("dsadas", brand, 2.5));
        vehicleModels.add(new ScooterModel("2w9", brand, 2.5));
        vehicleModels.add(new ScooterModel("ds88", brand, 2.8));
        vehicleModels.add(new BicycleModel("dsa45", brand, 10));

        Collections.sort(vehicleModels);
        System.out.println(vehicleModels);
    }
}
