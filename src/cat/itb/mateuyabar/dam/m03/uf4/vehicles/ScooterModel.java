package cat.itb.mateuyabar.dam.m03.uf4.vehicles;

public class ScooterModel extends VehicleModel{
    double power;

    public ScooterModel(String name, VehicleBrand vehicleBrand, double power) {
        super(name, vehicleBrand);
        this.power = power;
    }

    @Override
    public String toString() {
        return "ScooterModel{" +
                "power=" + power +
                ", name='" + name + '\'' +
                ", vehicleBrand=" + vehicleBrand +
                '}';
    }
}
