package cat.itb.mateuyabar.dam.m03.uf1.iterative;

public class MultiplyTableFull {
    public static void main(String[] args) {
        for(int i=1; i<10; i++) {
            for (int j = 1; j < 10; j++) {
                if(i*j<10)
                    System.out.print(" ");
                System.out.print(j * i+" ");
            }
            System.out.println();
        }
    }
}
