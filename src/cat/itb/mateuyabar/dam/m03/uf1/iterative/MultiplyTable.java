package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        int i = 1;
        while(i<=9){
            int result = number * i;
            System.out.println(i+" * "+number+" = "+result);
            i++;
        }
    }
}
