package cat.itb.mateuyabar.dam.m03.uf1.iterative.practice;

import java.util.Scanner;

public class SetIMig {
    public static void main(String[] args) {
        double userScore = playUserTurn();
        if(userScore<=7.5){
            double computerScore = playComputerTurn(userScore);
            if(userScore>computerScore){
                // guanya usuari
            } else {
                //guanya PC
            }
        } else {
            // guanya pc, t'has passat
        }
    }

    /**
     * Given the number of a card, it returns the value in a 7iMig game. 8, 9 and 10 represents jack, horse,
     * king respectively.
     * @param card between 1 and 10
     * @return value of the gien card.
     */
    public static double getCardValue(int card){
        if(card>=1 && card<=7)
            return card;
        else
            return 0.5;
    }

    /**
     * Given the number of a card, it returns the name of the card. 8, 9 and 10 represents jack (sota), horse (cavall),
     * king (rei) respectively.
     * @param card between 1 and 10
     * @return name of the given card.
     */
    public static String getCardText(int card){
        switch (card){
            case 8:
                return "sota";
            case 9:
                return "cavall";
            case 10:
                return "rei";
            default:
                return card+"";
        }
    }

    /**
     * Plays the computer turn.
     * @param userCounter user turn result. The computer will try to obtain at least the same value.
     * @return result of the sum of the computer cards value (even if greater than 7.5)
     */
    public static double playComputerTurn(double userCounter){
        double computerScore = 0;
        while (computerScore<userCounter && computerScore < 7.5){
            int card = ItbRandomizer.nextInt(11);
            double cardValue = getCardValue(card);
            computerScore += cardValue;
        }

        return computerScore;
    }

    /**
    * Plays the user turn.
    *
    * @return result of the sum of the user cards value (even if greater than 7.5)
    */
    public static double playUserTurn() {
        Scanner scanner = new Scanner(System.in);
        double score = 0;
        int wantsToPlay = 1;
        while(score<7.5 && wantsToPlay==1){
            int card = ItbRandomizer.nextInt(10);
            double cardValue = getCardValue(card);
            score+=cardValue;
            wantsToPlay = scanner.nextInt();
        }
        return score;
    }
}
