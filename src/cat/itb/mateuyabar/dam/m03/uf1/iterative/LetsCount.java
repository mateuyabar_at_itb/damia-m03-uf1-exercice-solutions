package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class LetsCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int maxNumber = scanner.nextInt();

        int i = 1;
        while(i<=maxNumber) {
            System.out.print(i);
            i++;
        }

    }
}
