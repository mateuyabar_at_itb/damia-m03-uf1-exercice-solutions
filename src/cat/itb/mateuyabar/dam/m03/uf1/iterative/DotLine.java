package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class DotLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int dots = scanner.nextInt();

        for(int printedDots=0; printedDots<dots; printedDots++){
            System.out.print(".");
        }
    }
}
