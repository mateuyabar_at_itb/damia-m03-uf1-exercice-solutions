package cat.itb.mateuyabar.dam.m03.uf1.iterative;

import java.util.Scanner;

public class HowManyLines {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String line = scanner.nextLine();
        int lineCount = 0;
        while(!line.equals("END")){
            lineCount++;
            line = scanner.nextLine();
        }
        System.out.println(lineCount);
    }
}
