package cat.itb.mateuyabar.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SumaParells {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int num = scanner.nextInt();
        boolean pair = false;
        int parells = 0;
        int senars = 0;
        while(num!=-1){
            if(pair)
                parells+=num;
            else
                senars+=num;

            pair = !pair;
            num = scanner.nextInt();
        }

    }
}
