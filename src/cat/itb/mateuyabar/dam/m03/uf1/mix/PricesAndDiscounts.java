package cat.itb.mateuyabar.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PricesAndDiscounts {
    public static void main(String[] args) {
        List<Integer> prices = new ArrayList<>();
        List<Integer> dicounts = new ArrayList<>();
        List<Integer> discountedPrices = new ArrayList<>();

        Scanner scanner = new Scanner(System.in);
        int price = scanner.nextInt();
        while(price!=-1){
            prices.add(price);
            price = scanner.nextInt();
        }

        int discount = scanner.nextInt();
        while(discount!=-1){
            dicounts.add(discount);
            discount = scanner.nextInt();
        }

        for(int i =0; i<prices.size(); ++i){
            price = prices.get(i);
            discount = dicounts.get(i);
            int finalPrice = price-(price*discount)/100;
            discountedPrices.add(finalPrice);
        }
        System.out.println(discountedPrices);
    }
}
