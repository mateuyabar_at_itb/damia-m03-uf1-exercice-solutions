package cat.itb.mateuyabar.dam.m03.uf1.mix;

import java.util.Scanner;

public class RadarFilter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int counter = 0;

        int speed = scanner.nextInt();
        while(speed!=-1) {
            if (speed > 90)
                counter++;
            speed = scanner.nextInt();
        }
    }
}
