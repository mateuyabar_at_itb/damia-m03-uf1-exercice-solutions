package cat.itb.mateuyabar.dam.m03.uf1.mix;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String palindom = scanner.nextLine();
        palindom = palindom.replace(" ", "")
                .replace(".", "")
                .replace(",", "")
                .replace("!", "")
                .replace("?", "")
                .replace("-", "");

        boolean result = true;
        for(int i = 0; i< palindom.length()/2; ++i){
            char endChar = palindom.charAt(palindom.length()-i-1);
            if(palindom.charAt(i)!=endChar) {
                result = false;
                break;
            }
        }
        System.out.println(result);
    }
}
