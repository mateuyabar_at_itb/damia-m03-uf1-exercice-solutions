package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class InverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = new int[10];
        for(int i=0; i<10; ++i){
            values[i] = scanner.nextInt();
        }

        for(int i = 9 ; i>=0 ; i--){
            int value = values[i];
            System.out.println(value);
        }
    }
}

