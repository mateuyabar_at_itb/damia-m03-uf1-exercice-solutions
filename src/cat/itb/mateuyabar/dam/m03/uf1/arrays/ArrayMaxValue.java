package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArrayMaxValue {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);

        int maxValue = values[0];
        for(int value : values){
            if(value>maxValue)
                maxValue = value;
        }
    }
}
