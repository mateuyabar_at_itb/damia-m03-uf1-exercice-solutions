package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class SimpleBattleshipResult {
    public static void main(String[] args) {
        boolean[][] ships ={
            {true, true, false, false, false, false, true},
            {false, false, true, false, false, false, true},
            {false, false, false, false, false, false, true},
            {false, true, true, true, false, false, true},
            {false, false, false, false, true, false, false},
            {false, false, false, false, true, false, false},
            {true, false, false, false, false, false, false}};

        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        boolean ship = ships[x][y];

        if(ship){
            System.out.println("tocat");
        } else {
            System.out.println("aigua");
        }
    }
}
