package cat.itb.mateuyabar.dam.m03.uf1.arrays;

public class IsThereA5 {
    public static void main(String[] args) {
        int[] values = {4,8,9,40,54,84,40,6,84,1,1,68,84,68,4,840,684,25,40,98,54,687,31,4894,468,46,84687,894,40,846,1681,618,161,846,84687,6,848};

        boolean result = false;
        for(int i = 0; i < values.length; ++i){
            int value = values[i];
            if(value % 7 == 0){
                result = true;
                break;
            }
        }
        System.out.println(result);
    }
}
