package cat.itb.mateuyabar.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayConfigurator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = new int[10];

        int position = scanner.nextInt();
        while(position!=-1){
            int value = scanner.nextInt();
            values[position] = value;

            position = scanner.nextInt();
        }

        System.out.println(Arrays.toString(values));




    }
}
