package cat.itb.mateuyabar.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ChessRockMatrix {
    public static void main(String[] args) {
        boolean[][] board = new boolean[8][8];

        Scanner scanner = new Scanner(System.in);
        int x = scanner.nextInt();
        int y = scanner.nextInt();

        for(int i = 0; i<8; ++i){
            board[x][i] = true;
            board[i][y] = true;
        }
        board[x][y] = false;

        System.out.println(Arrays.deepToString(board));
    }
}
