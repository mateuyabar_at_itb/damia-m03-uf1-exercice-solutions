package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class DayOfWeek {
    public static void main(String[] args) {
        String[] daysOfWeek = {"dilluns", "dimarts", "dimecres",
                "dijous", "divendres", "dissabte", "diumenge"};

        Scanner scanner = new Scanner(System.in);
        int dayNumber = scanner.nextInt();
        String day = daysOfWeek[dayNumber];

        System.out.println(day);
    }
}
