package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySortedValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);

        boolean sorted = true;
        for(int i=1; i<values.length; ++i){
            if(values[i]<values[i-1]){
                sorted = false;
                break;
            }
        }
        if(sorted)
            System.out.println("ordenat");
    }
}
