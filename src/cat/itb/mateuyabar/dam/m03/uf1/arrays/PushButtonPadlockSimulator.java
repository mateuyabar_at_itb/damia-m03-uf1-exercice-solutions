package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class PushButtonPadlockSimulator {
    public static void main(String[] args) {
        boolean[] padlock = new boolean[8];
        Scanner scanner = new Scanner(System.in);

        int buttonClicked = scanner.nextInt();
        while(buttonClicked!=-1) {
            padlock[buttonClicked] = !padlock[buttonClicked];
            buttonClicked = scanner.nextInt();
        }


    }
}
