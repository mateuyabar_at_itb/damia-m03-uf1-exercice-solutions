package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySumValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);

        int result  = 0;
        for(int value : values){
            result += value;
        }
        System.out.println(result);
    }
}
