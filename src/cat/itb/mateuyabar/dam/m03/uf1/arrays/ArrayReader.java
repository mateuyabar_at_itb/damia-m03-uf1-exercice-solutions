package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArrayReader {
    /**
     * Reads an int of arrays from user input.
     * The user first introduces the number (N).
     * Later it introduces the integers one by one.
     * @return int array of values introduced of size N
     */
    public static int[] scannerReadIntArray(Scanner scanner){
        int size = scanner.nextInt();
        int[] values = new int[size];
        for(int i =0; i<size; ++i){
            values[i] = scanner.nextInt();
        }
        return values;
    }
}
