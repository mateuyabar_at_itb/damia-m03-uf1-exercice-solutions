package cat.itb.mateuyabar.dam.m03.uf1.arrays;

import java.util.Scanner;

public class MinOf10Values {
    public static void main(String[] args) {
        int[] values = new int[10];

        Scanner scanner = new Scanner(System.in);

        for(int i=0; i<10; ++i){
            int value = scanner.nextInt();
            values[i] = value;
        }

        int minValue = values[0];
        for(int i = 1; i < 10; ++i){
            minValue = Math.min(minValue, values[i]);
        }

        System.out.println(minValue);

    }
}
