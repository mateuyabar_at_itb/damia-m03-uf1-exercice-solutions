package cat.itb.mateuyabar.dam.m03.uf1.arrays.exam;

public class HowManyMountains {
    public static void main(String[] args) {
        double[][] map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},{1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}};
        int mountainCounter = 0;
        for(int i = 1; i<map.length-1; ++i){
            for(int j= 1; j<map[i].length-1; ++j){
                boolean top = map[i][j] >= map[i][j-1];
                boolean bottom = map[i][j] >= map[i][j+1];
                boolean left = map[i][j] >= map[i-1][j];
                boolean right = map[i][j] >= map[i+1][j];
                if(top && bottom && left && right){
                    mountainCounter++;
                }
            }
        }

        System.out.println(mountainCounter);
    }
}
