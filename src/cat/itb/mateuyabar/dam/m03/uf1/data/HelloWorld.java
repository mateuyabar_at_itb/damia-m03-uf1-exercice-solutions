package cat.itb.mateuyabar.dam.m03.uf1.data;

public class HelloWorld {
    /**
     *  Prints a welcome message
     */
    public static void main(String[] args) {
        // Code to be executed
        System.out.println("Hello World");
    }
}