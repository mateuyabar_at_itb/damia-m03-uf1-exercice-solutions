package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

public class IntRectangleArea {
    public static void main(String[] args) {
        // llegir llargada i amplada
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int width = scanner.nextInt();

        // llargada * amplada
        int area = calculateRectagleArea(length, width);

        // imprimir resultat
        System.out.println(area);
    }

    private static int calculateRectagleArea(int length, int width) {
        return length * width;
    }
}
