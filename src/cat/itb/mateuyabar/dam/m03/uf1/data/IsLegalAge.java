package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

/**
 * L'usuari escriu un enter amb la seva
 * edat i s'imprimeix true si és major
 * d'edat, i false en qualsevol altre cas.
 */
public class IsLegalAge {
    public static void main(String[] args) {
        // Ask age
        Scanner scanner = new Scanner(System.in);
        int age = scanner.nextInt();

        // age >= 18 ?
        boolean isLegalAge = age >= 18;

        // print result
        System.out.println(isLegalAge);
    }
}
