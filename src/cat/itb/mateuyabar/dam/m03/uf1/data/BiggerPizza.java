package cat.itb.mateuyabar.dam.m03.uf1.data;

import java.util.Scanner;

public class BiggerPizza {
    public static void main(String[] args) {
        // Ask user diameter of pizza
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix el diametre de la pizza");
        double diameter = scanner.nextDouble();
        // Ask user width and length of pizza

        System.out.println("Introdueix els costats de la pizza rectangular");
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();

        // calculate area of rounded pizza
        double pi = 3.1415926535898;
        double radius = diameter / 2;
        double areaRoundedPizza = pi * radius * radius;

        // calculate area of rectangle pizza
        double areaRectanglePizza = width * height;

        // compare areas
        boolean isRoundedBigger = areaRoundedPizza > areaRectanglePizza;

        // print result
        System.out.println("La pizza rodona és més gran?");
        System.out.println(isRoundedBigger);
    }
}
