package cat.itb.mateuyabar.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> values = new ArrayList<>();
        int value =  scanner.nextInt();
        boolean addToStart = true;
        while(value!=-1) {
            if(addToStart)
                values.add(0, value);
            else
                values.add(value);
            addToStart = !addToStart;
            value =  scanner.nextInt();
        }
    }

}
