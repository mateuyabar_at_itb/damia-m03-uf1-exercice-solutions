package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class IntRectangleAreaFunc {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int height = scanner.nextInt();
        int width = scanner.nextInt();

        int area = rectangleArea(height, width);

        System.out.println(area);
    }

    private static int rectangleArea(int height, int width) {
        return height * width;
    }


}
