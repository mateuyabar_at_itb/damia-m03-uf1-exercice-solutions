package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class HiddenNumber {
    public static void main(String[] args) {
        System.out.println("Introdueix un enter");
        Scanner scanner = new Scanner(System.in);
        int userValue = scanner.nextInt();

        int computerValue = calculateComputerNumber();
        if(computerValue==userValue){
            System.out.println("L'has encertat");
        } else {
            System.out.println("No l'has encertat");
        }
    }

    private static int calculateComputerNumber() {
        return (int) (Math.random() * 2)+1;
    }
}
