package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

import static cat.itb.mateuyabar.dam.m03.uf1.staticfunctions.BiggerPizzaFunc.isRoundedBigger;

public class WhichPizzaShouldIBuyFunc {
    public static void main(String[] args) {
        // Ask user for diameter
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix el diametre de la pizza");
        double diameter = scanner.nextDouble();

        // Ask user for rectangle sides
        System.out.println("Introdueix els dos costats de la pizza");
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();

        String result = whichPizzaShouldIBuy(diameter, width, height);
        System.out.println(result);
    }

    private static String whichPizzaShouldIBuy(double diameter, double width, double height) {
        boolean roundedBigger = isRoundedBigger(diameter, width, height);
        if(roundedBigger){
            return "Compra la rodona";
        } else {
            return "Compra la rectangular";
        }
    }
}
