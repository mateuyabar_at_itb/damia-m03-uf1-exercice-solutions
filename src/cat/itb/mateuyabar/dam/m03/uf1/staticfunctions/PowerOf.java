package cat.itb.mateuyabar.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class PowerOf {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();

        int result = (int) Math.pow(a,b);

        System.out.println(result);
    }
}
