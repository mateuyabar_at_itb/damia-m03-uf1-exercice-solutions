package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class AbsoluteNumber {
    public static void main(String[] args) {
        // Ask for a integer
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix un enter");
        int value = scanner.nextInt();

        // calculate absolute value
        int result;
        if(value > 0){
            result = value;
        } else {
            result = -value;
        }
        System.out.println("El valor absolut de "+value+" és: ");
        System.out.println(result);
    }
}
