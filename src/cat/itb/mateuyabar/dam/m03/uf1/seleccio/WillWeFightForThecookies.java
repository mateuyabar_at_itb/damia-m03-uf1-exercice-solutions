package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WillWeFightForThecookies {
    public static void main(String[] args) {
        // Ask for number of people and cookies
        Scanner scanner = new Scanner(System.in);
        System.out.println("Quantes persones sou?");
        int peopleCount = scanner.nextInt();
        System.out.println("Quantes galetes teniu?");
        int cookiesCount = scanner.nextInt();

        // Can I divide the cookies?
        boolean canDivideCookies = cookiesCount % peopleCount == 0;

        // Print result
        String message;
        if(canDivideCookies){
            message = "Let's Eat!";
        } else {
            message = "Let's Fight";
        }
        System.out.println(message);
    }
}
