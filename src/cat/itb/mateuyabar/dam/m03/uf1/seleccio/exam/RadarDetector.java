package cat.itb.mateuyabar.dam.m03.uf1.seleccio.exam;

public class RadarDetector {
    public static void main(String[] args) {
        int speed = 45;
        if(speed>140){
            System.out.println("Multa greu");
        } else if(speed>120){
            System.out.println("Multa lleu");
        } else {
            System.out.println("Correcte");
        }
    }
}
