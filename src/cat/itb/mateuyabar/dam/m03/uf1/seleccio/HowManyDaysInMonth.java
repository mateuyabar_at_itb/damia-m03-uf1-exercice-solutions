package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class HowManyDaysInMonth {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter month number");
        int month = scanner.nextInt();
        int days;

        switch (month){
            case 1: case 3: case 5: // TODO ...
                days = 31;
                break;
            case 2:
                days = 28;
                break;
            default:
                days = 30;
                break;
        }


        System.out.println(days);

    }
}
