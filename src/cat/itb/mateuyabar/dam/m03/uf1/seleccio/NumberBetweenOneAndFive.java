package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number;
        do {
            System.out.println("Introdueix un número del 1 al 5");
            number = scanner.nextInt();
        } while(number<1 || number>5);

        System.out.println("El número introduït: "+number);
    }
}
