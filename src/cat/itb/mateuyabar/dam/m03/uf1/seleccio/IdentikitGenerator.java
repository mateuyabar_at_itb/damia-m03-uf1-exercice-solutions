package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IdentikitGenerator {
    public static void main(String[] args) {
        // ask user for description
        Scanner scanner = new Scanner(System.in);
        String hair = scanner.nextLine();
        String eyes = scanner.nextLine();
        String nose = scanner.nextLine();
        String mouth = scanner.nextLine();

        // calculate image
        // arrissats @@@@@, llisos VVVVV o pentinats XXXXX
        String hairImage;
        switch (hair){
            case "arrissats":
                hairImage = "@@@@@";
                break;
            case "llisos":
                hairImage = "VVVVV";
                break;
            case "pentinats":
                hairImage = "XXXXX";
                break;
            default:
                hairImage = "ERROR";
        }
        // aclucats .-.-., rodons .o-o. o estrellats _.*-_."
        String eyesImage;
        switch (eyes){
            case "aclucats":
                eyesImage = ".-.-.";
                break;
            case "rodons":
                eyesImage = ".o-o.";
                break;
            case "estrellats":
                eyesImage = ".*-*.";
                break;
            default:
                eyesImage = "ERROR";
        }
        // TODO nose and mouth

        // print image
        System.out.println(hairImage);
        System.out.println(eyesImage);
        // TODO sout nose and mouth
    }
}
