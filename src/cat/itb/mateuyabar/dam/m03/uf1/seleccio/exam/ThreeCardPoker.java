package cat.itb.mateuyabar.dam.m03.uf1.seleccio.exam;

public class ThreeCardPoker {
    public static void main(String[] args) {
        int card1 = 1;
        int card2 = 1;
        int card3 = 1;

        if(card1==card2 && card2 == card3){
            System.out.println("Trio");
        } else if(card1==card2 || card2 == card3 || card1 == card3){
            System.out.println("Parella");
        } else if(card1 == card2-1 && card2 == card3-1 ||
                card1 == card3-1 && card3 == card2-1
                //...
        ){ // casos no ordenat
            System.out.println("Escala");
        } else {
            System.out.println("Número alt");
        }

    }
}

