package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class WhichBigger {
    public static void main(String[] args) {
        // ask two integer
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix 2 enter");
        int value1 = scanner.nextInt();
        int value2 = scanner.nextInt();

        // compare them
        boolean isValue1Bigger = value1 > value2;

        int result;
        if(isValue1Bigger){
            result = value1;
        } else {
            result = value2;
        }
        System.out.println(result);

        // print bigger
    }
}
