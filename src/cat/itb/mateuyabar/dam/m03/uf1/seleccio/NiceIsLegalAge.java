package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class NiceIsLegalAge {
    public static void main(String[] args) {
        // Ask age
        Scanner scanner = new Scanner(System.in);
        int age = scanner.nextInt();

        // age >= 18 ?
        boolean isLegalAge = age >= 18;

        if(isLegalAge)
            System.out.println("ets major d'edat");
    }
}
