package cat.itb.mateuyabar.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class OnlyVowels {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberLetters = scanner.nextInt();

        for(int i=0; i<numberLetters; ++i){
            char character = scanner.nextLine().charAt(0);
            if(isVowel(character)){
                System.out.println(character);
            }
        }
    }

    private static boolean isVowel(char character) {
        return character=='a'|| character=='e'|| character=='i'|| character=='o'|| character=='u';
    }
}
